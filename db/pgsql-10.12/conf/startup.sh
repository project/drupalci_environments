#!/bin/bash

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

PGVERSION=$(/usr/bin/psql --version | awk '{print $3}' | head -n1 | cut -c 1-5)
PGMAJOR=$(echo ${PGVERSION} | cut -c 1-2)
echo "PGSQL VERSION: ${PGVERSION}"

if [ ! -z $(pg_lsclusters | grep -c ' main ') ];
    then
    echo "rebuilding PostgreSQL database cluster"
    # stop and drop the cluster
    pg_dropcluster ${PGMAJOR} main --stop
    # create a fresh new cluster
    pg_createcluster ${PGMAJOR} main --start -d /var/lib/postgresql/${PGVERSION}/main

    # create a new user with CREATEDB permissions
    psql -c "CREATE USER drupaltestbot WITH PASSWORD 'drupaltestbotpw' CREATEDB;"
    # create a new default database for the user
    psql -c "CREATE DATABASE drupaltestbot OWNER drupaltestbot TEMPLATE DEFAULT ENCODING='utf8' LC_CTYPE='en_US.UTF-8' LC_COLLATE='en_US.UTF-8';"
    # create the extension pg_trgm
    psql -d drupaltestbot -c "CREATE EXTENSION pg_trgm;"
    # create an additional schema for the database for testing only
    psql -d drupaltestbot -c "CREATE SCHEMA IF NOT EXISTS testing_fake AUTHORIZATION drupaltestbot;"
    # stop the cluster
    pg_ctlcluster ${PGMAJOR} main stop
    # allow md5-based password auth for IPv4 connections
    echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/${PGMAJOR}/main/pg_hba.conf
    # copy conf after it was deleted by pg_dropcluster
    cp /opt/postgresql.conf /etc/postgresql/${PGMAJOR}/main/postgresql.conf
    mkdir -p /var/lib/postgresql/${PGVERSION}/main.pg_stat_tmp
fi

/usr/lib/postgresql/${PGMAJOR}/bin/postgres -D /var/lib/postgresql/${PGVERSION}/main -c config_file=/etc/postgresql/${PGMAJOR}/main/postgresql.conf
echo "pgsql died at $(date)";
