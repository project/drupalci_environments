#!/bin/bash

terminate() {
    echo "Caught termination signal, shutting down..."
    if [ -n "${PID}" ]; then
        kill -TERM "${PID}"
        wait "${PID}"
    fi
    echo "Shutdown complete."
    exit 0
}

trap terminate SIGTERM SIGINT

if [ ! -f /var/lib/mysql/ibdata1 ];
    then
    echo "rebuilding /var/lib/mysql/ibdata1"
    /usr/sbin/mysqld --initialize-insecure --user=mysql --basedir=/usr/ --datadir=/var/lib/mysql/
    test -e /var/run/mysqld || install -m 755 -o mysql -g root -d /var/run/mysqld
    /usr/sbin/mysqld &
    PID="${!}"
    while ! mysqladmin ping; do sleep 1; done
    mysql -e "CREATE USER 'drupaltestbot'@'%' IDENTIFIED BY 'drupaltestbotpw';"
    mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'drupaltestbot'@'%' WITH GRANT OPTION; SELECT User FROM mysql.user; FLUSH PRIVILEGES;"
    echo "Grants added"
fi

# Start MySQL server if container restart and data is ready
if [ -z "${PID}" ]; then
    /usr/sbin/mysqld &
    PID="${!}"
fi
wait ${PID}
echo "mysql exited at $(date)";
