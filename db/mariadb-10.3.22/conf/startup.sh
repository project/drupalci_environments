#!/bin/bash

terminate() {
    echo "Caught termination signal, shutting down..."
    if [ -n "${PID}" ]; then
        kill -TERM "$(pgrep -P ${PID} -x mysqld)"
        wait "${PID}"
    fi
    echo "Shutdown complete."
    exit 0
}

trap terminate SIGTERM SIGINT

if [ ! -f /var/lib/mysql/ibdata1 ];
    then
    echo "rebuilding /var/lib/mysql/ibdata1"
    mysql_install_db --user=mysql --basedir=/usr/ --datadir=/var/lib/mysql/
    test -e /var/run/mysqld || install -m 755 -o mysql -g root -d /var/run/mysqld
    /usr/bin/mysqld_safe &
    PID="${!}"
    while ! netcat -vz localhost 3306; do sleep 1; done
    mysql -e "CREATE USER 'drupaltestbot'@'%' IDENTIFIED BY 'drupaltestbotpw';"
    mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'drupaltestbot'@'%' WITH GRANT OPTION; SELECT User FROM mysql.user; FLUSH PRIVILEGES;"
    echo "Grants added"
fi

# Start MySQL server if container restart and data is ready
if [ -z "${PID}" ]; then
    /usr/bin/mysqld_safe &
    PID="${!}"
fi
wait ${PID}
echo "mysql exited at $(date)";
