#!/bin/bash

mongod --dbpath /data/db1 --port 27017 --fork --logpath /var/log/mongodb/db1.log --replSet dbrs --keyFile /replica.key --bind_ip_all --transitionToAuth
mongod --dbpath /data/db2 --port 27018 --fork --logpath /var/log/mongodb/db2.log --replSet dbrs --keyFile /replica.key --bind_ip_all --transitionToAuth
mongod --dbpath /data/db3 --port 27019 --fork --logpath /var/log/mongodb/db3.log --replSet dbrs --keyFile /replica.key --bind_ip_all --transitionToAuth

echo "###### Waiting for mongo primary member instance startup.."
until mongosh --host localhost:27017 --eval 'quit(db.runCommand({ ping: 1 }).ok ? 0 : 2)' &>/dev/null; do
  printf '.'
  sleep 1
done
echo "###### Working mongo primary member instance found, initiating user setup & initializing rs setup.."

# setup the replica sets
mongosh --host localhost:27017 <<EOF
var config = {
    "_id": "dbrs",
    "version": 1,
    "members": [
        {
            "_id": 1,
            "host": "localhost:27017",
            "priority": 2
        },
        {
            "_id": 2,
            "host": "localhost:27018",
            "priority": 1
        },
        {
            "_id": 3,
            "host": "localhost:27019",
            "priority": 1,
            "arbiterOnly": true
        }
    ]
};
rs.initiate(config, { force: true });
exit;

EOF

# The MongoDB needs a moment for the replica set setup.
sleep 10

# Setup the database user for Drupal.
mongosh --host localhost:27017 <<EOF
use admin;
var rootUser = 'db';
var rootPassword = 'db';
db.createUser({
     user: rootUser,
     pwd: rootPassword,
     roles: [
       {role: "dbOwner", db: "admin"},
       {role: "dbOwner", db: "db"},
    ]
});
use db;
db.createUser({
     user: rootUser,
     pwd: rootPassword,
     roles: [
       {role: "dbOwner", db: "admin"},
       {role: "dbOwner", db: "db"},
    ]
});

rs.status();
exit;

EOF

tail -f /var/log/mongodb/db*.log
