######
# Apache Setup
######
ifelse(
PHP_NUM,`73',`RUN apt-get update && apt-get install -y apache2 --no-install-recommends && rm -rf /var/lib/apt/lists/*',
PHP_NUM,`74',`RUN apt-get update && apt-get install -y apache2 --no-install-recommends && rm -rf /var/lib/apt/lists/*',
PHP_NUM,`80',`RUN apt-get update && apt-get install -y apache2 --no-install-recommends && rm -rf /var/lib/apt/lists/*',
PHP_NUM,`81',`RUN apt-get update && apt-get install -y apache2 --no-install-recommends && rm -rf /var/lib/apt/lists/*',
PHP_NUM,`82',`RUN apt-get update && apt-get install -y apache2 --no-install-recommends && rm -rf /var/lib/apt/lists/*',`RUN apt-get update && apt-get install -y apache2-bin apache2.2-common apache2-dbg --no-install-recommends && rm -rf /var/lib/apt/lists/*')

RUN set -ex \
	\
	&& sed -ri 's/^export ([^=]+)=(.*)$/: ${\1:=\2}\nexport \1/' /etc/apache2/envvars \
	&& sed -i 's/Require local/#Require local/' /etc/apache2/mods-available/status.conf \
	\
	&& . /etc/apache2/envvars \
	&& echo "ServerName localhost" >> /etc/apache2/apache2.conf \
	&& for dir in \
		"$APACHE_LOCK_DIR" \
		"$APACHE_RUN_DIR" \
		"$APACHE_LOG_DIR" \
		/var/www/html \
		/var/www/apc \
	; do \
		rm -rvf "$dir" \
		&& mkdir -p "$dir" \
		&& chown -R "$APACHE_RUN_USER:$APACHE_RUN_GROUP" "$dir"; \
	done

# Apache + PHP requires preforking Apache for best results
RUN a2dismod mpm_event && a2enmod mpm_prefork

# PHP files should be handled by PHP, and should be preferred over any other file type
RUN { \
		echo '<FilesMatch \.php$>'; \
		echo '\tSetHandler application/x-httpd-php'; \
		echo '</FilesMatch>'; \
		echo; \
		echo 'DirectoryIndex disabled'; \
		echo 'DirectoryIndex index.php index.html'; \
		echo; \
		echo '<Directory /var/www/>'; \
		echo '\tOptions -Indexes'; \
		echo '\tAllowOverride All'; \
		echo '</Directory>'; \
	} | tee /etc/apache2/conf-available/docker-php.conf \
	&& a2enconf docker-php

COPY ./conf/apache2/vhost.conf /etc/apache2/sites-available/drupal.conf
COPY ./apache2-foreground /usr/local/bin/

RUN a2enmod rewrite \
    && a2dissite 000-default.conf \
    && a2ensite drupal
