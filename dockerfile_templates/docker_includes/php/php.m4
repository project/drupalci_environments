define(`ARCHIVE_EXTENSION', ifelse(PHP_NUM,`54',tar.gz, PHP_TYPE,`branch',zip,PHP_TYPE,`release',tar.xz))dnl
define(`DOWNLOAD_URL',
ifelse(
PHP_NUM,`82',`https://php.net/distributions/php-8.2.0.tar.xz',
PHP_TYPE,`branch',`https://github.com/php/php-src/archive/PHP-'PHP_VERSION`.'ARCHIVE_EXTENSION,
PHP_TYPE,`release',`https://secure.php.net/get/php-'PHP_VERSION`.'ARCHIVE_EXTENSION`/from/this/mirror" PHP_ASC_URL="https://secure.php.net/get/php-'PHP_VERSION`.'ARCHIVE_EXTENSION`.asc/from/this/mirror'))dnl
ifelse(
PHP_NUM,`56',`########',
PHP_NUM,`70',`########',
PHP_NUM,`71',`########',
PHP_NUM,`72',`########',
PHP_NUM,`73',`########',
PHP_NUM,`74',`######',
PHP_NUM,`80',`######',
PHP_NUM,`81',`#######',
PHP_NUM,`82',`#########',`######')
# Php Setup
######
ifelse(
PHP_NUM,`81',`RUN echo "deb http://deb.debian.org/debian bullseye-backports main" | tee /etc/apt/sources.list.d/bullseye-backports.list',
PHP_NUM,`82',`RUN echo "deb http://deb.debian.org/debian bullseye-backports main" | tee /etc/apt/sources.list.d/bullseye-backports.list',`dnl')
ifelse(
PHP_NUM,`81',`RUN echo "deb http://deb.debian.org/debian buster main" | tee /etc/apt/sources.list.d/buster.list',`dnl')
ifelse(
PHP_NUM,`53',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`54',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`55',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`56',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`70',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`71',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`72',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`73',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`74',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`80',`RUN apt-get update && apt-get install -y --no-install-recommends \',
PHP_NUM,`81',`RUN apt-get update && apt-get install -y --no-install-recommends --allow-downgrades \',
PHP_NUM,`82',`RUN apt-get update && apt-get install -y --no-install-recommends \',`dnl')
ifelse(
PHP_NUM,`53',`        autoconf2.13 \',`        autoconf \')
ifelse(PHP_TYPE,`branch',`		bison \',`dnl')
		ca-certificates \
		curl \
		file \
		g++ \
		gcc \
		gdb \
ifelse(
PHP_NUM,`73',`		gnupg2 \',
PHP_NUM,`74',`		gnupg2 \',
PHP_NUM,`80',`		gnupg2 \',
PHP_NUM,`81',`		gnupg2 \',
PHP_NUM,`82',`		gnupg2 \',`dnl')
		libc-dev \
		libedit2 \
ifelse(
PHP_NUM,`53',`		librecode0 \',
PHP_NUM,`54',`		librecode0 \',`dnl')
ifelse(PHP_TYPE,`release',`		libsqlite3-0 \',`dnl')
ifelse(PHP_TYPE,`release',`		libxml2 \',`dnl')
		make \
		pkg-config \
		re2c \
ifelse(PHP_TYPE,`release',`		xz-utils \',`dnl')
ifelse(PHP_TYPE,`branch',`		unzip \',`dnl')
		&& rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/local/etc/php/conf.d

ENV PHP_CFLAGS="-O2 -g"
ENV PHP_CPPFLAGS="$PHP_CFLAGS"
ENV PHP_LDFLAGS=""

ENV GPG_KEYS 1729F83938DA44E27BA0F4D3DBDB397470D12172 B1B44D8F021E4E2D6021E995DC9FF8D3EE5AF27F 0B96609E270F565C13292B24C13C70B87267B52D 0BD78B5F97500D450838F95DFE857D9A90D90EC1 F38252826ACD957EF380D39F2F7956BC5DA04B5D 6E4F6AB321FDC07F2C332E3AC2BF0BC433CFC8B3 1A4E8B7277C42E53DBA9C7B9BCAA30EA9C0D5763 A917B1ECDA84AEC2B568FED6F50ABC807BD5DCD0 528995BFEDFBA7191D46839EF9BA0ADA31CBD89E 0A95E9A026542D53835E3F3A7DEC4E69FC9C83D7 CBAF69F173A0FEA4B537F470D66C9593118BCCB6 42670A7FE4D0441C8E4632349E4FDC074A4EF02D 5A52880781F755608BF815FC910DEB46F53EA312

define(`KEY_SERVER',
ifelse(
PHP_NUM,`56',`pool.sks-keyservers.net',
PHP_NUM,`70',`pool.sks-keyservers.net',
PHP_NUM,`71',`pool.sks-keyservers.net',
PHP_NUM,`72',`pool.sks-keyservers.net',
PHP_NUM,`73',`pool.sks-keyservers.net',
PHP_NUM,`74',`pool.sks-keyservers.net',
PHP_NUM,`80',`pool.sks-keyservers.net',
PHP_NUM,`81',`pool.sks-keyservers.net',
PHP_NUM,`82',`pool.sks-keyservers.net',`pool.sks-keyservers.net'))dnl
# PHP Version
ENV `PHP_VERSION' PHP_VERSION
ENV PHP_URL="DOWNLOAD_URL"

RUN set -xe; \
	\
	fetchDeps=' \
		wget \
	'; \
	apt-get update; \
        ifelse(
        PHP_NUM,`53',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`54',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`55',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`56',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`70',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`71',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`72',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`73',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`74',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`80',`apt-get install -y --no-install-recommends $fetchDeps; \',
        PHP_NUM,`81',`apt-get install -y --no-install-recommends --allow-downgrades $fetchDeps; \',
        PHP_NUM,`82',`apt-get install -y --no-install-recommends $fetchDeps; \',`dnl')
	rm -rf /var/lib/apt/lists/*; \
	\
	mkdir -p /usr/src; \
	cd /usr/src; \
	\
	wget -O php.ARCHIVE_EXTENSION "$`PHP_URL'"; \
	\
	if [ -n "$`PHP_ASC_URL'" ]; then \
		wget -O php.ARCHIVE_EXTENSION.asc "$`PHP_ASC_URL'"; \
		#export GNUPGHOME="$(mktemp -d)"; \
		#for key in $`GPG_KEYS'; do \
		# Since the keyservers are completly unreliable, I have to comment them out. I guess we might get malware.
			#gpg --keyserver KEY_SERVER --recv-keys "$key"; \
		#done; \
		#gpg --batch --verify php.ARCHIVE_EXTENSION.asc php.ARCHIVE_EXTENSION; \
		#rm -r "$GNUPGHOME"; \
	fi; \
	\
	apt-get purge -y --auto-remove $fetchDeps

COPY docker-php-source /usr/local/bin/

RUN set -xe \
	&& buildDeps=" \
        apache2-dev \
        libcurl4-openssl-dev \
        libedit-dev \
        libfreetype6-dev \
        libicu-dev \
        libjpeg62-turbo-dev \
ifelse(
PHP_NUM,`74',`        libonig-dev \
        libkrb5-dev \
        libpng-dev \
        zlib1g-dev \',
PHP_NUM,`80',`        libonig-dev \
        libkrb5-dev \
        libpng-dev \
        zlib1g-dev \',
PHP_NUM,`81',`        libonig-dev \
        libkrb5-dev \
        libpng-dev \
        zlib1g-dev \',
PHP_NUM,`82',`        libonig-dev \
        libkrb5-dev \
        libpng-dev \
        zlib1g-dev \',`dnl')
        libmcrypt-dev \
ifelse(
PHP_NUM,`73',`        libpng-dev \',
PHP_NUM,`74',`        libpng-dev \',
PHP_NUM,`80',`        libpng-dev \',
PHP_NUM,`81',`        libpng-dev \',
PHP_NUM,`82',`        libpng-dev \',`        libpng12-dev \')
ifelse(
PHP_NUM,`71',`        libwebp-dev \',
PHP_NUM,`72',`        libwebp-dev \',
PHP_NUM,`73',`        libwebp-dev \',
PHP_NUM,`74',`        libwebp-dev \',
PHP_NUM,`80',`        libwebp-dev \',
PHP_NUM,`81',`        libwebp-dev \',
PHP_NUM,`82',`        libwebp-dev \',`dnl')
ifelse(
PHP_NUM,`81',`        libavif-dev/bullseye-backports \',
PHP_NUM,`82',`        libavif-dev/bullseye-backports \',`dnl')
ifelse(
PHP_NUM,`81',`        libavif13/bullseye-backports \',
PHP_NUM,`82',`        libavif13/bullseye-backports \',`dnl')
ifelse(
PHP_NUM,`81',`        libaom3/bullseye-backports \',
PHP_NUM,`82',`        libaom3/bullseye-backports \',`dnl')
ifelse(
PHP_NUM,`53',`        libsqlite3-dev \',
PHP_NUM,`54',`        libsqlite3-dev \',
PHP_NUM,`55',`        libsqlite3-dev \',
PHP_NUM,`56',`        libsqlite3-dev \',
PHP_NUM,`70',`        libsqlite3-dev \',
PHP_NUM,`71',`        libsqlite3-dev \',
PHP_NUM,`72',`        libsqlite3-dev \',
PHP_NUM,`73',`        libsqlite3-dev \',
PHP_NUM,`74',`        libsqlite3-dev \',
PHP_NUM,`80',`        libsqlite3-dev \',
PHP_NUM,`81',`        libsqlite3-dev/buster \',
PHP_NUM,`82',`        libsqlite3-dev \',`dnl')
        libpq-dev \
        libssl-dev \
        libtidy-dev \
        libxml2-dev \
        libxslt1-dev \
        libyaml-dev \
ifelse(
PHP_NUM,`73',`        libzip-dev \',
PHP_NUM,`74',`        libzip-dev \',
PHP_NUM,`80',`        libzip-dev \',
PHP_NUM,`81',`        libzip-dev \',
PHP_NUM,`82',`        libzip-dev \',`dnl')
        ncurses-dev \
ifelse(
PHP_NUM,`53',`		bzip2 \
		libreadline6-dev \
		librecode-dev \',
PHP_NUM,`54',`		bzip2 \
		libreadline6-dev \
		librecode-dev \',`dnl')
    " \
	&& apt-get update && apt-get install -y $buildDeps --no-install-recommends --allow-downgrades && rm -rf /var/lib/apt/lists/* \
	\
	&& docker-php-source extract \
	&& cd /usr/src/php \
ifelse(
PHP_NUM,`53',`	&& mkdir -p /usr/include/freetype2/freetype \
	&& ln -s /usr/include/freetype2/freetype.h /usr/include/freetype2/freetype/freetype.h \',`dnl')
	&& export CFLAGS="-O2 -g" \
		CPPFLAGS="-O2 -g" \
		LDFLAGS="" \
ifelse(
PHP_NUM,`53',`	&& ./buildconf --force
RUN set -xe \
	&& cd /usr/src/php \',`dnl')
ifelse(PHP_TYPE,`branch',`	&& ./buildconf \',`dnl')
	&& ./configure \
            --with-config-file-path=/usr/local/etc/php \
            --with-config-file-scan-dir=/usr/local/etc/php/conf.d \
            --enable-ftp \
ifelse(
PHP_NUM,`53',`            --with-libdir=/lib/x86_64-linux-gnu \',`dnl')
            --enable-mbstring \
            --enable-mysqlnd \
            --with-curl \
            --with-libedit \
            --with-zlib \
            --with-kerberos \
ifelse(
PHP_NUM,`53',`            --with-openssl=/usr \',`            --with-openssl \')
            --with-mysql=mysqlnd \
            --with-mysqli=mysqlnd \
            --with-pdo-mysql=mysqlnd \
            --with-pdo-sqlite \
            --with-pdo-pgsql \
ifelse(
PHP_NUM,`56',`            --enable-phpdbg \',
PHP_NUM,`70',`            --enable-phpdbg \',
PHP_NUM,`71',`            --enable-phpdbg \',
PHP_NUM,`72',`            --enable-phpdbg \',
PHP_NUM,`73',`            --enable-phpdbg \',
PHP_NUM,`74',`            --enable-phpdbg \',
PHP_NUM,`80',`            --enable-phpdbg \',
PHP_NUM,`81',`            --enable-phpdbg \',
PHP_NUM,`82',`            --enable-phpdbg \',`dnl')
            --with-readline \
ifelse(
PHP_NUM,`53',`            --with-recode \',
PHP_NUM,`54',`            --with-recode \',`dnl')
ifelse(
PHP_NUM,`74',`dnl',
PHP_NUM,`80',`dnl',
PHP_NUM,`81',`dnl',
PHP_NUM,`82',`dnl',`            --with-png-dir \')
ifelse(
 PHP_NUM,`73',`            --with-freetype \',
 PHP_NUM,`74',`            --with-freetype \',
 PHP_NUM,`80',`            --with-freetype \',
 PHP_NUM,`81',`            --with-freetype \',
 PHP_NUM,`82',`            --with-freetype \',`            --with-freetype-dir \')
            --with-zlib-dir \
ifelse(
 PHP_NUM,`74',`            --with-jpeg \',
 PHP_NUM,`80',`            --with-jpeg \',
 PHP_NUM,`81',`            --with-jpeg \',
 PHP_NUM,`82',`            --with-jpeg \',`            --with-jpeg-dir \')
            --with-mcrypt \
            --with-xsl \
            --with-tidy \
            --with-xmlrpc \
            --with-gettext=shared \
ifelse(
PHP_NUM,`74',`            --enable-gd \',
PHP_NUM,`80',`            --enable-gd \',
PHP_NUM,`81',`            --enable-gd \',
PHP_NUM,`82',`            --enable-gd \',`            --with-gd \')
ifelse(
PHP_NUM,`71',`            --with-webp-dir \',
PHP_NUM,`72',`            --with-webp-dir \',
PHP_NUM,`73',`            --with-webp-dir \',`dnl')
ifelse(
PHP_NUM,`74',`            --with-webp \',
PHP_NUM,`80',`            --with-webp \',
PHP_NUM,`81',`            --with-webp \',
PHP_NUM,`82',`            --with-webp \',`dnl')
ifelse(
PHP_NUM,`81',`            --with-avif \',
PHP_NUM,`82',`            --with-avif \',`dnl')
            --with-pear \
            --enable-sockets \
            --enable-exif \
ifelse(
 PHP_NUM,`74',`            --with-zip \',
 PHP_NUM,`80',`            --with-zip \',
 PHP_NUM,`81',`            --with-zip \',
 PHP_NUM,`82',`            --with-zip \',`            --enable-zip \')
            --enable-soap \
            --enable-sysvsem \
            --enable-cgi \
            --enable-sysvshm \
            --enable-shmop \
            --enable-pcntl \
            --enable-bcmath \
            --enable-xmlreader \
ifelse(
PHP_NUM,`53',`            --enable-intl=shared \',`            --enable-intl \')
ifelse(
 PHP_NUM,`74',`dnl',
 PHP_NUM,`80',`dnl',
 PHP_NUM,`81',`dnl',
 PHP_NUM,`82',`dnl',`            --enable-wddx \')
ifelse(
PHP_NUM,`53',`dnl',`            --enable-opcache \')
            --with-apxs2 \
    && make -j "$(nproc)" \
    && make install \
ifelse(
PHP_NUM,`53',`    && make clean \
    && dpkg -r bison libbison-dev',`    && make clean')


COPY docker-php-ext-* docker-php-entrypoint /usr/local/bin/

# install pecl extensions for apcu, xdebug, and yaml
define(`PECL_DEPS',
ifelse(
PHP_NUM,`53',`APC-3.1.13 xdebug-2.2.7',
PHP_NUM,`54',`APC-3.1.13 xdebug-2.4.1',
PHP_NUM,`55',`APCu-4.0.11 xdebug-2.5.5 yaml-1.3.1',
PHP_NUM,`56',`APCu-4.0.11 xdebug-2.5.5 yaml-1.3.1',
PHP_NUM,`70',`APCu-5.1.17 xdebug-2.7.2 yaml-2.0.4',
PHP_NUM,`71',`APCu-5.1.18 xdebug-2.9.6 yaml-2.1.0',
PHP_NUM,`72',`APCu-5.1.18 xdebug-2.9.6 yaml-2.1.0',
PHP_NUM,`73',`APCu-5.1.19 xdebug-2.9.6 yaml-2.2.0',
PHP_NUM,`74',`APCu-5.1.19 xdebug-2.9.6 yaml-2.2.0',
PHP_NUM,`80',`APCu-5.1.19 yaml-2.2.0',
PHP_NUM,`81',`APCu-5.1.20',
PHP_NUM,`82',`APCu-5.1.20'))dnl
RUN docker-php-ext-pecl-install PECL_DEPS \
changequote(<!,!>)dnl
ifelse(
PHP_NUM, <!80!>,<!dnl!>,
PHP_NUM, <!81!>,<!dnl!>,
PHP_NUM, <!82!>,<!dnl!>,<!    && sed -i 's/^/;/' /usr/local/etc/php/conf.d/docker-php-pecl-xdebug.ini \!>)
changequote(`,')dnl
    && apt-get update \
ifelse(
PHP_NUM,`73',`    && apt-get install -y default-mysql-client postgresql-client sudo git sqlite3 --no-install-recommends \',
PHP_NUM,`74',`    && apt-get install -y default-mysql-client postgresql-client sudo git sqlite3 --no-install-recommends \',
PHP_NUM,`80',`    && apt-get install -y default-mysql-client postgresql-client sudo git sqlite3 --no-install-recommends \',
PHP_NUM,`81',`    && apt-get install -y default-mysql-client postgresql-client sudo git sqlite3/buster --allow-downgrades --no-install-recommends \',
PHP_NUM,`82',`    && apt-get install -y default-mysql-client postgresql-client sudo git sqlite3 --no-install-recommends \',`    && apt-get install -y mysql-client postgresql-client sudo git sqlite3 --no-install-recommends \')
    && rm -rf /var/lib/apt/lists/*
define(`APC_VERSION',
ifelse(
PHP_NUM,`55',`v4.0.11',
PHP_NUM,`56',`v4.0.11',
PHP_NUM,`70',`v5.1.17',
PHP_NUM,`71',`v5.1.17',
PHP_NUM,`72',`v5.1.17',
PHP_NUM,`73',`v5.1.17',
PHP_NUM,`74',`v5.1.17'))dnl
changequote(<!,!>)
define(<!APC_SCRIPT!>, <!RUN curl https://raw.githubusercontent.com/krakjoe/apcu/APC_VERSION/apc.php > /var/www/apc/apc.php \
    && sed -i "s/'USE_AUTHENTICATION',1/'USE_AUTHENTICATION',0/" /var/www/apc/apc.php \
    && sed -i "s#////////// READ OPTIONAL CONFIGURATION FILE ////////////#ini_set('memory_limit', '-1');\n////////// READ OPTIONAL CONFIGURATION FILE ////////////#" /var/www/apc/apc.php!>)dnl
changequote(`,')dnl
ifelse(
PHP_NUM,55,`APC_SCRIPT',
PHP_NUM,56,`APC_SCRIPT',
PHP_NUM,70,`APC_SCRIPT',
PHP_NUM,71,`APC_SCRIPT',
PHP_NUM,72,`APC_SCRIPT',
PHP_NUM,73,`APC_SCRIPT',
PHP_NUM,74,`APC_SCRIPT',`dnl')

COPY ./conf/php/php.ini /usr/local/etc/php/php.ini
COPY ./conf/php/php-cli.ini /usr/local/etc/php/php-cli.ini
