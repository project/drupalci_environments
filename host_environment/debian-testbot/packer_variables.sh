#!/bin/bash
# Debian Jessie ISO (sha512 checksum)
export PACKER_DEBIAN_ISO_URL="/usr/local/drupalci_environments/debian-8.10.0-amd64-netinst.iso"
export PACKER_DEBIAN_ISO_SUM="367f99ede326de69f1e434aa6e9e5517a3437ee1239bba76dd0d53c3cb912507c660b684ed41d7ef80fbe93d2099ae1d3209bfca5df3dd90b1b5a486ce9c1369"
export DEBIAN_FRONTEND=noninteractive

# User to be created
export PACKER_SSH_USER="testbot"
export PACKER_SSH_PASS="testbot"

# VirtualBox additions ISO (sha256 checksum)
export PACKER_VBOX_ISO_URL="/usr/share/virtualbox/VBoxGuestAdditions.iso"
export PACKER_VBOX_ISO_SUM="e5b425ec4f6a62523855c3cbd3975d17f962f27df093d403eab27c0e7f71464a"

# AWS credentials
# not declared here because they're sourced from AWS config files
