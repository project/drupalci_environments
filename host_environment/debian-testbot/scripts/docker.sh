#!/bin/bash -eux

# Name:        docker.sh
# Author:      Nick Schuch (nick@myschuch.com)
# Description: Installs Docker.
date
export DEBIAN_FRONTEND=noninteractive


echo "Installing Docker"
# New way of installing
apt-get install -y  \
     gnupg2 \
     software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   jessie \
   stable"

apt-get -qq update
apt-get install -y docker-ce=17.12.0~ce-0~debian


# We also need to add the "testbot" user to the docker group so it can run
# containers.
usermod -a -G docker testbot

service docker stop
echo '{
    "storage-driver": "devicemapper"
}' >> /etc/docker/daemon.json
rm -rf /var/lib/docker/*
service docker start

