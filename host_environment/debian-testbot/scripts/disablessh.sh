#!/bin/bash -eux

# This will prevent SSH from starting up so that we have time to set up the tmpfs before jenkins tries to get on the
# instance and start copying files. sshd is then re-enabled in /etc/rc.local which is modified by drupalci.sh
touch /etc/ssh/sshd_not_to_be_run

