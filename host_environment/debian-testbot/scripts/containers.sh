#!/bin/bash -eux

# TODO: this should iterate over containers that are stored at dockerhub in the drupalci account.
# This would require using their API somehow, which might mean that we'd have to have a
echo "Pulling Containers"
docker pull drupalci/mysql-5.5
docker pull drupalci/mysql-5.7
docker pull drupalci/pgsql-9.1
docker pull drupalci/pgsql-9.5
docker pull drupalci/pgsql-9.6
docker pull drupalci/pgsql-10.12
docker pull drupalci/pgsql-12.1
docker pull drupalci/pgsql-13.4
docker pull drupalci/pgsql-14.0

#pull new db containers
docker pull drupalci/db-mysql-5.5:dev
docker pull drupalci/db-mysql-5.5:production
docker pull drupalci/db-mysql-5.7:dev
docker pull drupalci/db-mysql-5.7:production
docker pull drupalci/db-pgsql-9.1:dev
docker pull drupalci/db-pgsql-9.1:production
docker pull drupalci/db-pgsql-9.5:dev
docker pull drupalci/db-pgsql-9.5:production
docker pull drupalci/db-pgsql-9.6:dev
docker pull drupalci/db-pgsql-9.6:production
docker pull drupalci/db-pgsql-10.12:dev
docker pull drupalci/db-pgsql-10.12:production
docker pull drupalci/db-pgsql-12.1:dev
docker pull drupalci/db-pgsql-12.1:production
docker pull drupalci/db-pgsql-13.4:dev
docker pull drupalci/db-pgsql-13.4:production
docker pull drupalci/db-pgsql-14.0:dev
docker pull drupalci/db-pgsql-14.0:production
#pull php containers
docker pull drupalci/php-5.3.29-apache:dev
docker pull drupalci/php-5.3.29-apache:production
docker pull drupalci/php-5.4.45-apache:dev
docker pull drupalci/php-5.4.45-apache:production
docker pull drupalci/php-5.5.38-apache:dev
docker pull drupalci/php-5.5.38-apache:production
docker pull drupalci/php-5.6-apache:dev
docker pull drupalci/php-5.6-apache:production
docker pull drupalci/php-7.0-apache:dev
docker pull drupalci/php-7.0-apache:production
docker pull drupalci/php-7.1-apache:dev
docker pull drupalci/php-7.1-apache:production
docker pull drupalci/php-7.1.x-apache:dev
docker pull drupalci/php-7.1.x-apache:production
docker pull drupalci/php-7.2-apache:dev
docker pull drupalci/php-7.2-apache:production
docker pull drupalci/php-7.2.x-apache:dev
docker pull drupalci/php-7.2.x-apache:production
docker pull drupalci/php-7.3-apache:dev
docker pull drupalci/php-7.3-apache:production
docker pull drupalci/php-7.3.x-apache:dev
docker pull drupalci/php-7.3.x-apache:production

docker pull drupalci/chromedriver:dev
docker pull drupalci/chromedriver:production
docker pull drupalci/webdriver-chromedriver:dev
docker pull drupalci/webdriver-chromedriver:production

# new containers.
#for CONTAINER in $(find ./containers -name Dockerfile | grep -v 'dev' | awk -F"/" '{print $4}');
#do
#   docker pull drupalci/${CONTAINER};
#done
