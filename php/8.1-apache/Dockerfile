######
# Base
######
ARG THE_BASE_IMAGE=debian:bullseye-slim
ARG PHPIZE_DEPS="autoconf file g++ gcc make pkg-config re2c"
ARG PHP_CFLAGS="-O2 -g"

FROM ${THE_BASE_IMAGE} as builder
ENV DRUPALCI=TRUE TERM=xterm DEBIAN_FRONTEND=noninteractive

#########
# Php build
######

# PHP Version
ENV PHP_VERSION 8.1.31
ENV PHP_URL="https://php.net/distributions/php-${PHP_VERSION}.tar.xz"
ENV PHP_SHA256_HASH="c4f244d46ba51c72f7d13d4f66ce6a9e9a8d6b669c51be35e01765ba58e7afca  php.tar.xz"

ARG PHPIZE_DEPS
ARG PHP_CFLAGS
ENV PHP_CFLAGS="$PHP_CFLAGS"
ENV PHP_CPPFLAGS="$PHP_CFLAGS"
ENV PHP_LDFLAGS=""

RUN set -eux &&\
    echo 'APT::Install-Recommends "0";' >/etc/apt/apt.conf.d/99norecommends &&\
    apt-get update && apt-get install -qy --no-install-recommends \
        ca-certificates curl apt-utils &&\
    \
    mkdir -p /usr/src &&\
    cd /usr/src &&\
    \
    curl -LS -o php.tar.xz "$PHP_URL" &&\
    \
    echo $PHP_SHA256_HASH | sha256sum -c &&\
    \
    buildDeps=" \
        apache2-dev apache2 \
        libcurl4-openssl-dev \
        libedit-dev \
        libfreetype6-dev \
        libicu-dev \
        libjpeg62-turbo-dev \
        libonig-dev \
        libkrb5-dev \
        zlib1g-dev \
        libpng-dev \
        libwebp-dev \
        libpq-dev \
        libssl-dev \
        libtidy-dev \
        libxml2-dev \
        libxslt1-dev \
        libyaml-dev \
        libzip-dev \
        ncurses-dev \
    " &&\
    apt-get install -qy --no-install-recommends $PHPIZE_DEPS $buildDeps &&\
    \
    echo "deb http://deb.debian.org/debian bullseye-backports main" | tee /etc/apt/sources.list.d/bullseye-backports.list &&\
    echo "deb http://deb.debian.org/debian buster main" | tee /etc/apt/sources.list.d/buster.list &&\
    apt-get update &&\
    apt-get install -qy --no-install-recommends --allow-downgrades \
        libavif-dev/bullseye-backports \
        libavif13/bullseye-backports \
        libaom3/bullseye-backports \
        libsqlite3-dev/buster \
    &&\
    rm -rf /var/lib/apt/lists/*

COPY ./conf/php/php.ini /usr/local/etc/php/php.ini
COPY ./conf/php/php-cli.ini /usr/local/etc/php/php-cli.ini
COPY ./docker-php-* /usr/local/bin/

RUN set -xe &&\
    buildDir="/usr/src/php" &&\
    mkdir -p /usr/local/etc/php/conf.d &&\
    \
    mkdir "$buildDir" &&\
    tar -Jxf /usr/src/php.tar.xz -C "$buildDir" --strip-components=1 &&\
    cd "$buildDir" &&\
    CFLAGS="$PHP_CFLAGS" \
    CPPFLAGS="$PHP_CPPFLAGS" \
    LDFLAGS="$PHP_LDFLAGS" \
    ./configure \
        --with-config-file-path=/usr/local/etc/php \
        --with-config-file-scan-dir=/usr/local/etc/php/conf.d \
        --enable-ftp \
        --enable-mbstring \
        --enable-mysqlnd \
        --with-curl \
        --with-libedit \
            --with-zlib \
            --with-kerberos \
            --with-openssl \
            --with-mysqli=mysqlnd \
            --with-pdo-mysql=mysqlnd \
            --with-pdo-sqlite \
            --with-pdo-pgsql \
            --with-readline \
            --with-freetype \
            --with-jpeg \
            --with-xsl \
            --with-tidy \
            --with-gettext=shared \
            --enable-gd \
            --with-webp \
            --with-avif \
            --with-pear \
            --enable-sockets \
            --enable-exif \
            --with-zip \
            --enable-soap \
            --enable-sysvsem \
            --enable-sysvshm \
            --enable-shmop \
            --enable-pcntl \
            --enable-bcmath \
            --enable-xmlreader \
            --enable-intl \
            --enable-opcache \
            --with-apxs2 \
            --disable-cgi \
            --disable-phpdbg \
           &&\
    make -j "$(nproc)" &&\
    make install &&\
    cd / && rm -fr "$buildDir"

# install pecl extensions for apcu, pcov, xdebug, and yaml
RUN pecl channel-update pecl.php.net &&\
    pecl install APCu-5.1.23 pcov-1.0.11 xdebug-3.3.2 yaml-2.2.3 \
    &&\
# stript .debug files out of executables
    echo '\
        for file in "$@"; do \
            objcopy --only-keep-debug "$file" "$file".debug; \
            strip --strip-debug --strip-unneeded "$file"; \
            objcopy --add-gnu-debuglink="$file".debug "$file"; \
        done' > /strip.sh; \
    sh strip.sh \
      /usr/lib/apache2/modules/libphp.so \
      /usr/local/bin/php \
      $(php -r 'echo ini_get("extension_dir");')/*.so


#########
# Php Setup
######

FROM ${THE_BASE_IMAGE}
ARG DEBIAN_FRONTEND=noninteractive
ENV DRUPALCI=TRUE TERM=xterm

COPY --from=builder /usr/local /usr/local

COPY --from=builder /usr/lib/apache2/modules/libphp.so /usr/lib/apache2/modules/libphp.so.debug /usr/lib/apache2/modules/
COPY --from=builder /etc/apache2/mods-enabled/php.load /etc/apache2/mods-enabled/php.load
COPY --from=builder /etc/apache2/mods-available/php.load /etc/apache2/mods-available/php.load

RUN set -xe &&\
    echo 'APT::Install-Recommends "0";' >/etc/apt/apt.conf.d/99norecommends ;\
    apt-get update &&\
    buildDeps=" \
        libedit2 \
        libfreetype6 \
        libicu67 \
        libjpeg62-turbo \
        libonig5 \
        libkrb5-3 \
        libpng16-16 \
        zlib1g \
        libwebp6 \
        libpq5 \
        libtidy5deb1 \
        libyaml-0-2 \
        libxml2 \
        libxslt1.1 \
        libzip4 \
        libncurses6 \
    " &&\
    runDeps=" \
        apache2 \
        bzip2 \
        curl ca-certificates gnupg2 \
        default-mysql-client postgresql-client sudo git sqlite3 \
        patch \
        rsync \
        unzip \
        xz-utils \
    " &&\
    apt-get install -qy --no-install-recommends $buildDeps $runDeps &&\
    \
    echo "deb http://deb.debian.org/debian bullseye-backports main" | tee /etc/apt/sources.list.d/bullseye-backports.list &&\
    echo "deb http://deb.debian.org/debian buster main" | tee /etc/apt/sources.list.d/buster.list &&\
    apt-get update &&\
    apt-get install -qy --no-install-recommends --allow-downgrades \
        libavif13/bullseye-backports \
        libaom3/bullseye-backports \
        libsqlite3-0/buster \
        sqlite3/buster \
        yq jq \
    &&\
    rm -rf /var/lib/apt/lists/*

# Install Composer, Drush
RUN curl -sSLo /tmp/composer-setup.php https://getcomposer.org/installer &&\
    curl -sSLo /tmp/composer-setup.sig https://composer.github.io/installer.sig &&\
    php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" &&\
    php /tmp/composer-setup.php --filename composer --install-dir /usr/local/bin &&\
    curl -sSLo /usr/local/bin/drush https://github.com/drush-ops/drush/releases/download/8.3.5/drush.phar &&\
    chmod +x /usr/local/bin/drush &&\
    /usr/local/bin/drush --version

# Install nodejs and yarn
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/trusted.gpg.d/nodesource.gpg &&\
    echo 'deb [signed-by=/etc/apt/trusted.gpg.d/nodesource.gpg] https://deb.nodesource.com/node_18.x nodistro main' | tee /etc/apt/sources.list.d/nodesource.list &&\
    curl -sSLo /etc/apt/trusted.gpg.d/yarn.gpg.asc https://dl.yarnpkg.com/debian/pubkey.gpg &&\
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list &&\
    apt-get update &&\
    apt-get install -qy --no-install-recommends nodejs yarn &&\
    rm -rf /var/lib/apt/lists/*

# Install phantomjs, supervisor
RUN _file=phantomjs-2.1.1-linux-x86_64 &&\
    curl -sSLo /$_file.tar.bz2 https://bitbucket.org/ariya/phantomjs/downloads/$_file.tar.bz2 &&\
    tar -jxf /$_file.tar.bz2 -C / &&\
    mv /$_file/bin/phantomjs /usr/bin/phantomjs &&\
    rm -f /$_file.tar.bz2 &&\
    rm -rf /$_file &&\
    chmod 755 /usr/bin/phantomjs &&\
    apt-get update &&\
    apt-get install -qy --no-install-recommends supervisor fontconfig &&\
    rm -rf /var/lib/apt/lists/*

COPY ./conf/supervisor-phantomjs.conf /etc/supervisor/conf.d/phantomjs.conf


######
# Apache Setup
######

RUN set -ex \
	\
	&& sed -ri 's/^export ([^=]+)=(.*)$/: ${\1:=\2}\nexport \1/' /etc/apache2/envvars \
	&& sed -i 's/Require local/#Require local/' /etc/apache2/mods-available/status.conf \
	\
	&& . /etc/apache2/envvars \
	&& echo "ServerName localhost" >> /etc/apache2/apache2.conf \
	&& for dir in \
		"$APACHE_LOCK_DIR" \
		"$APACHE_RUN_DIR" \
		"$APACHE_LOG_DIR" \
		/var/www/html \
		/var/www/apc \
	; do \
		rm -rvf "$dir" \
		&& mkdir -p "$dir" \
		&& chown -R "$APACHE_RUN_USER:$APACHE_RUN_GROUP" "$dir"; \
	done

COPY ./conf/apache2/vhost.conf /etc/apache2/sites-available/drupal.conf
COPY ./apache2-foreground /usr/local/bin/

# Apache + PHP requires preforking Apache for best results
RUN a2dismod mpm_event && a2enmod mpm_prefork &&\
	a2enmod expires headers rewrite &&\
	\
# PHP files should be handled by PHP, and should be preferred over any other file type
	{ \
		echo '<FilesMatch \.php$>'; \
		echo '\tSetHandler application/x-httpd-php'; \
		echo '</FilesMatch>'; \
		echo; \
		echo 'DirectoryIndex disabled'; \
		echo 'DirectoryIndex index.php index.html'; \
		echo; \
		echo '<Directory /var/www/>'; \
		echo '\tOptions -Indexes'; \
		echo '\tAllowOverride All'; \
		echo '</Directory>'; \
	} | tee /etc/apache2/conf-available/docker-php.conf &&\
	a2enconf docker-php &&\
	a2dissite 000-default.conf &&\
	a2ensite drupal

ARG PHPIZE_DEPS
ENV PHPIZE_DEPS=$PHPIZE_DEPS

ARG PHP_CFLAGS
ENV PHP_CFLAGS="$PHP_CFLAGS"
ENV PHP_CPPFLAGS="$PHP_CFLAGS"
ENV PHP_LDFLAGS=""

ENTRYPOINT ["docker-php-entrypoint"]

WORKDIR /var/www/html

EXPOSE 80
CMD ["apache2-foreground"]
